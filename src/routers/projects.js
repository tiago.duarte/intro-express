import express from 'express';
var { ObjectId } = require('mongodb');
var router = express.Router();

import { setupDB } from '../db';
import e from 'express';

let db;
//função para reaproveitar a conexão ativa com o bd
async function database() {
  try {
    if (db) {
      return db;
    } else {
      const db = await setupDB();
      return db;
    }
  } catch (error) {
    console.log(error);
  }
}

//acrescentado para facilitar visualização de testes manuais com o postman
router.get('/', async (req, res) => {
  db = await database(db);
  const existingProjects = await db.collection('projects').find({}).toArray();
  if (!existingProjects) {
    res.status(400).json({ error: `projects do not exist` });
  } else {
    res.json(existingProjects);
  };
});

router.post('/', async (req, res) => {
  db = await database(db);
  const { name } = req.body;

  const slug = name.toLowerCase();

  const project = {
    slug,
    boards: [],
  };

  const existingProject = await db.collection('projects').findOne({ slug });

  if (existingProject) {
    res.status(400).json({ error: `project ${slug} already exists` });
  } else {
    await db.collection('projects').insertOne(project);
    res.json({ project });
  }
});

const findProject = async (req, res, next) => {
  db = await database(db);
  const { slug } = req.params;

  const project = await db.collection('projects').findOne({ slug });

  if (!project) {
    res.status(400).json({ error: `project ${slug} does not exist` });
  } else {
    req.body.project = project;
    next();
  }
};

router.get('/:slug', findProject, async (req, res) => {
  const { project } = req.body;
  res.json({ project });
});

router.post('/:slug/boards', findProject, async (req, res) => {
  db = await database(db);
  const { name, project } = req.body;
  const board = { name, tasks: [] };
  project.boards.push(board);

  await db
    .collection('projects')
    .findOneAndReplace({ slug: project.slug }, project);

  res.json({ board });
});

//parte da lista 3
router.post('/:slug/boards/:name/tasks', findProject, async (req, res) => {
  db = await database(db);
  const { slug } = req.body.project;
  const { description } = req.body;
  const { name } = req.params;
  let existingProject = req.body.project;
  let existingBoard = existingProject.boards.find(board => {
    return board.name === name;
  })
  if(!existingBoard){
    res.status(400).json({ error: `board ${name} does not exist` });
  }
  else{
    const indexBoard = existingProject.boards.findIndex((board) => board.name === name);
    const task_id = existingProject.boards[indexBoard].tasks.length + 1;
    const task = { task_id, description }
    existingProject.boards[indexBoard].tasks.push(task);

    await db.collection('projects').findOneAndReplace({ slug: existingProject.slug }, existingProject);
    res.json(task);
  }
});

//parte da lista 3
router.delete('/:slug', findProject, async (req, res) => {
  db = await database(db);
  const { _id, slug } = req.body.project;
  await db.collection('projects').deleteOne({ "_id": ObjectId(_id) });
  res.json({ info: `project ${slug} deleted` });
});

//parte da lista 3
router.delete('/:slug/boards/:name', findProject, async (req, res) => {
  db = await database(db);
  const { _id, slug } = req.body.project;
  const { name } = req.params;
  let existingProject = req.body.project;
  let existingBoard = existingProject.boards.find(board => {return board.name === name;})
  if(!existingBoard){
    res.status(400).json({ error: `board ${name} does not exist` });
  }
  else{
  await db.collection('projects').updateOne({ "_id": ObjectId(_id) }, { $pull: { "boards": { "name": name } } });
  res.json({ info: `board ${name} deleted` });
  }
});

//parte da lista 3
router.delete('/:slug/boards/:name/tasks/:id', findProject, async (req, res) => {
  db = await database(db);
  const { _id, slug } = req.body.project;
  let { name, id} = req.params;
  let existingProject = req.body.project;
  id = parseInt(id);
  let existingBoard = existingProject.boards.find(board => {return board.name === name;})
  if(!existingBoard){
    res.status(400).json({ error: `board ${name} does not exist` });
  }
  else{
    const indexBoard = existingProject.boards.findIndex((board) => board.name === name);
    let existingTask = existingProject.boards[indexBoard].tasks.find(task => {return task.task_id === id});
    if (!existingTask){
      res.status(400).json({ error: `task ${id} does not exist` });
    }
    else{
      const indexTask = existingProject.boards[indexBoard].tasks.findIndex(task => task.task_id === id);
      existingProject.boards[indexBoard].tasks.splice(indexTask, 1);
      await db.collection('projects').findOneAndReplace({ slug: existingProject.slug }, existingProject);
      res.json({ info: `task ${id} deleted from board ${name}` });
    }
  }
});





module.exports = router;