import request from 'supertest';

import { app, bootstrap } from '../src/app';

describe('our app', () => {
  let db;

  beforeAll(async () => (db = await bootstrap()));

  beforeEach(async () => {
    await db.dropDatabase(process.env.DB_NAME + '_test');
  });

  afterAll(async () => await db.close());

  // código pt. 1
  test('GET / returns a "hello world"', async () => {
    // const obj = ...
    // const body = obj.body;
    const { body } = await request(app).get('/');

    expect(body.message).toBeDefined();
    expect(body.message).toBe('hello world');
  });

  // código pt. 2
  test('POST /projects returns a new project slug', async () => {
    const { body } = await request(app)
      .post('/projects')
      .send({ name: 'Awesome' });

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt.2
  test('GET /projects/:slug returns a matching project', async () => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).get('/projects/awesome');

    expect(body.project).toBeDefined();
    expect(body.project.slug).toBe('awesome');
  });

  // código pt. 3
  test('POST /projects/:slug/boards adds a new board into a project', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    const { body } = await request(app)
      .post(`/projects/${slug}/boards`)
      .send({ name });

    expect(body.board).toBeDefined();
    expect(body.board.name).toBe(name);
  });

  //parte da lista 3
  test('DELETE /projects/:slug', async() => {
    await request(app).post('/projects').send({ name: 'Awesome' });

    const { body } = await request(app).delete('/projects/awesome');

    expect(body.info).toBeDefined();
    expect(body.info).toBe('project awesome deleted');
  });

  //parte da lista 3
  test('DELETE /projects/:slug/boards/:name', async() => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    await request(app).post(`/projects/${slug}/boards`).send({ name });

    const { body } = await request(app).delete(`/projects/${slug}/boards/${name}`);
    expect(body.info).toBeDefined();
    expect(body.info).toBe(`board ${name} deleted`);
  });

  //parte da lista 3
  test('POST /projects/:slug/boards/:name/tasks', async() => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    await request(app).post(`/projects/${slug}/boards`).send({ name });

    const description = 'task number 1';

    const { body } = await request(app).post(`/projects/${slug}/boards/${name}/tasks`).send({ description });

    expect(body).toBeDefined();
    expect(body.description).toBe(description);
  });

  //parte da lista 3
  test('DELETE /projects/:slug/boards/:name/tasks/:id', async () => {
    const project = { name: 'Awesome' };
    const {
      body: {
        project: { slug },
      },
    } = await request(app).post('/projects').send(project);

    const name = 'todo';

    await request(app).post(`/projects/${slug}/boards`).send({ name });

    const description = 'task number 1';

    await request(app).post(`/projects/${slug}/boards/${name}/tasks`).send({ description });

    const id = 1;

    const { body } = await request(app).delete(`/projects/${slug}/boards/${name}/tasks/${id}`);
    expect(body.info).toBeDefined();
    expect(body.info).toBe(`task ${id} deleted from board ${name}`);

  });
});

